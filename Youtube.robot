*** Settings ***
Library    SeleniumLibrary
Library    ExcelRobot
Suite Setup       Set Selenium Speed          5s
Test Setup        Open browser Youtube
Test Teardown     Close Browser
Suite Teardown    Close All Browsers

*** Variables ***
${path}    ${EXECDIR}/Keyword.xls
${sheetname}    Songlist
${url}                    https://www.youtube.com/
${browser}                gc
${locator_inputsearch}    name=search_query
${locator_result}         xpath=//*[@id="video-title"]/yt-formatted-string
${locator_menu}           xpath=//*[@id="guide-icon"]/yt-icon-shape/icon-shape/div
${locator_history}        xpath=//*[@href="/feed/history" and @id="endpoint" and @class="yt-simple-endpoint style-scope ytd-guide-entry-renderer"] 
${locator_ads}            xpath=//*[@class="ytp-ad-skip-button ytp-button"]
${locator_logoicon}       id=logo-icon



*** Keywords ***
Open browser Youtube
    Set Selenium Speed          0.5s
    Open browser                ${url}    ${browser}
    Wait Until Page Contains    YouTube
    Maximize Browser Window

Input data for test
    [Arguments]    ${song}
    Open browser Youtube
    Wait Until Page Contains    YouTube
    Input Text    ${locator_inputsearch}    ${song}
    Press Keys    ${locator_inputsearch}    ENTER
    Wait Until Element Is Visible    ${locator_result}
    Click Element    ${locator_result}
    ${present}=    Run Keyword And Return Status    Wait Until Element Is Visible    ${locator_ads}    7s
    Run Keyword If    ${present}    SkipAds
    Sleep    10s
    Click Element    ${locator_menu}
    Wait Until Element Is Visible    ${locator_history}
    Click Element    ${locator_history}
    Close Window

SkipAds
    Wait Until Element Is Visible    ${locator_ads}
    Click Element    ${locator_ads}

Get data from excel
    Open Excel    ${path}
    ${rowcount}=    Get Row Count    ${sheetname}
    FOR  ${item}  IN RANGE    0    ${rowcount}
        ${song}    Read Cell Data    ${sheetname}    0    ${item}   
        Input data for test    ${song}   
    END
    
Key Search
    Wait Until Page Contains         Search
    Input Text    ${locator_inputsearch}    ธาตุทองซาวด์
    Press Keys    ${locator_inputsearch}    ENTER

Play Viedo youtube
    Wait Until Element Is Visible    ${locator_result}
    Click Element    ${locator_result}
    ${present}=    Run Keyword And Return Status    Wait Until Element Is Visible    ${locator_ads}    7s
    Run Keyword If    ${present}    SkipAds2
    Sleep    10s

SkipAds2
    Wait Until Element Is Visible    ${locator_ads}
    Click Element    ${locator_ads}

View history youtube
    Click Element    ${locator_menu}
    Wait Until Element Is Visible    ${locator_history}
    Click Element    ${locator_history}


*** Test Cases ***
TC001 - Play viedo on youtube
    Key Search
    Play Viedo youtube

TC002 - View history
    Key Search
    Play Viedo youtube
    View history youtube

TC003 - Test run excel
    [Setup]
    Get data from excel
    [Teardown]
